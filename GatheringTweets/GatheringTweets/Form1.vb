﻿Imports System
Imports System.IO
Imports System.Net
Imports System.Web
Imports MySql.Data.MySqlClient



Public Class Form1
    Friend Shared flag, offset, dbIndex As Integer
    Friend Shared addTime As Double = 86400
    Friend Shared minTime, maxTime, endTime As Double
    Friend Shared keyword, currentDate As String
    Friend Shared firstDate As Double = 1356998403
    ', 1359676803, 1362096003, 1364774403, 1367366403, 1370044803, 1372636803, 1375315203, 1377993603, 1380585603, 1383264003, 1385856003, 1388610021}
    Friend Shared startDate As Date = New Date(2013, 1, 1, 0, 0, 0)
    Friend Shared sDate, eDate As Date
    Friend Shared filePath As String

    Public DBConnect1 As MySqlConnection

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        dbIndex = 0
        ' Show "hidden" text
        ListView1.ShowItemToolTips = True
        ' Set columnar mode
        ListView1.View = View.Details
        ' Set column header
        ListView1.Columns.Clear()
        ListView1.Columns.Add("Date", 120)
        ListView1.Columns.Add("Tweet", 1500)
        ' Remove previous items
        ListView1.Items.Clear()

    End Sub



    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim searchKeyword As String
        ListView1.Items.Clear()
        searchKeyword = TextBox1.Text
        getTime()
        Search_Twitter(searchKeyword)

        'Search_Twitter2(searchKeyword, month)

    End Sub

    Private Sub getTime()
        Dim mindex, eindex As Long
        mindex = DateDiff("d", startDate, sDate)
        eindex = DateDiff("d", sDate, eDate)
        minTime = firstDate + (mindex * addTime)
        endTime = minTime + (eindex * addTime)
        maxTime = minTime + addTime
    End Sub

    Private Sub Search_Twitter(input As String)

        flag = 1
        offset = 0
        keyword = input
        Dim url As String

        currentDate = sDate.ToString("yyyy-MM-dd")
        TextBox4.Text = currentDate

        url = "http://topsy.com/s?q=" + keyword + "&type=tweet&language=ko&offset=" + offset.ToString + "&mintime=" + minTime.ToString + "&maxtime=" + maxTime.ToString
        'Form2.Show()
        Form2.WebBrowser1.Navigate(New Uri(url))

    End Sub


    Public Sub setText()
        TextBox2.Clear()
        TextBox3.Clear()
        TextBox2.Text = offset / 10
        TextBox3.Text = ListView1.Items.Count
    End Sub

    Public Sub nextPage()
        offset = offset + 10
        Dim url As String = "http://topsy.com/s?q=" + keyword + "&type=tweet&language=ko&offset=" + offset.ToString + "&mintime=" + minTime.ToString + "&maxtime=" + maxTime.ToString
        Form2.WebBrowser1.Navigate(New Uri(url))
    End Sub

    Public Sub nextDay()
        insertDb()

        minTime = minTime + addTime

        If minTime < endTime Then
            flag = 1
            offset = 0
            sDate = sDate.AddDays(1)
            maxTime = maxTime + addTime
            currentDate = sDate.ToString("yyyy-MM-dd")
            TextBox4.Clear()
            TextBox4.Text = currentDate
            Dim url As String = "http://topsy.com/s?q=" + keyword + "&type=tweet&language=ko&offset=" + offset.ToString + "&mintime=" + minTime.ToString + "&maxtime=" + maxTime.ToString
            Form2.WebBrowser1.Navigate(New Uri(url))
        Else
            MsgBox("Search end")
        End If

    End Sub

    Private Sub insertDb()
        Dim sql As String
        Dim comm As New MySqlCommand
        sql = "INSERT INTO tweet_hyg (date, tweet, positive_index) values (@date,@tweet,@index)"
        DBConnect()
        comm.Connection = DBConnect1
        comm.CommandText = sql
        'comm.CommandType = CommandType.Text

        For index As Integer = dbIndex To (ListView1.Items.Count - 1)
            Dim tweetData As ListViewItem = ListView1.Items(index)
            Dim subData As ListViewItem.ListViewSubItem = tweetData.SubItems(1)
            comm.Parameters.AddWithValue("@date", currentDate)
            comm.Parameters.AddWithValue("@tweet", subData.Text)
            comm.Parameters.AddWithValue("@index", 0)
            comm.ExecuteNonQuery()
            comm.Parameters.Clear()
        Next
        DBClose()
        'dbIndex = ListView1.Items.Count
        ListView1.Items.Clear()
    End Sub

    Public Sub DBConnect()

        DBConnect1 = New MySqlConnection
        DBConnect1.ConnectionString = "Database=dykimpaper2;Data Source=gss.ssu.ac.kr;User Id=dykim;Password=1212"

        Try
            DBConnect1.Open()

            If Not DBConnect1.State = ConnectionState.Open Then
                MessageBox.Show("DB 연결 실패", "창이름", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Application.Exit()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
            MessageBox.Show("DB 연결 실패", "창이름", MessageBoxButtons.OK, MessageBoxIcon.Error)
            Application.Exit()
        End Try
    End Sub

    Public Sub DBClose()
        If DBConnect1.State = ConnectionState.Open Then
            DBConnect1.Close()
        End If
    End Sub

    Public Sub saveTweets()
        Dim directoryPath As String = "C:\tweets\" + keyword
        Dim path As String = directoryPath + filePath
        My.Computer.FileSystem.CreateDirectory(directoryPath)
        Dim fs As FileStream = File.Create(path)
        fs.Close()

        For Each tweetData As ListViewItem In ListView1.Items
            Dim subData As ListViewItem.ListViewSubItem
            subData = tweetData.SubItems(0)
            My.Computer.FileSystem.WriteAllText(path, subData.Text + ",", True)
            subData = tweetData.SubItems(1)
            My.Computer.FileSystem.WriteAllText(path, subData.Text + Environment.NewLine, True)
        Next

        Form2.Close()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        saveTweets()
    End Sub


    Private Sub DateTimePicker1_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker1.ValueChanged
        sDate = DateTimePicker1.Value
    End Sub

    Private Sub DateTimePicker2_ValueChanged(sender As Object, e As EventArgs) Handles DateTimePicker2.ValueChanged
        eDate = DateTimePicker2.Value
        filePath = "\" + sDate.ToString("MMdd") + "~" + eDate.ToString("MMdd") + ".txt"
    End Sub


End Class
