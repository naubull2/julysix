﻿Public Class Form2

    Private Sub WebBrowser1_DocumentCompleted(sender As Object, e As WebBrowserDocumentCompletedEventArgs) Handles WebBrowser1.DocumentCompleted
        Dim htmlDoc As New HtmlAgilityPack.HtmlDocument()
        htmlDoc.LoadHtml(WebBrowser1.Document.Body.InnerHtml)
        Dim tempTweet As String
        Dim tempData(1) As String
        Dim tempNode As ListViewItem
        Dim temp As HtmlAgilityPack.HtmlNodeCollection = htmlDoc.DocumentNode.SelectNodes("//*[@class='result-tweet']/div/div/div")


        If temp IsNot Nothing Then
            For Each tweet As HtmlAgilityPack.HtmlNode In temp
                tempTweet = tweet.InnerText
                If tempTweet.Contains("RT") = False Then
                    If Form1.ListView1.FindItemWithText(tempTweet) Is Nothing Then
                        tempData(0) = Form1.currentDate
                        tempData(1) = tempTweet
                        tempNode = New ListViewItem(tempData)
                        Form1.ListView1.Items.Add(tempNode)
                    End If
                End If

            Next
            Form1.setText()
        Else
            Form1.flag = 0
        End If


        If Form1.flag = 1 Then
            Form1.nextPage()
        Else
            Form1.nextDay()
        End If

    End Sub

    
End Class