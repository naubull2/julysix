package soma.sentiment;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

public class Dictionary {
	public ArrayList<String> word;
	public ArrayList<Float> positive;

	public String getWord(int index) {
		return word.get(index);
	}

	public Float getPositive(int index) {
		return positive.get(index);
	}

	public int contains(String inWord) {
		int index = -1;

		index = word.indexOf(inWord);

		return index;
	}

	public Dictionary() {
		word = new ArrayList<String>();
		positive = new ArrayList<Float>();

		try {
			BufferedReader in = new BufferedReader(new FileReader(
					"dic.csv"));
			String s;
			while ((s = in.readLine()) != null) {
				String[] temp = s.split(",");
				word.add(temp[0]);
				positive.add(Float.parseFloat(temp[1]));
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
