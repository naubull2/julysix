package soma.sentiment;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import soma.sentiment.tempDictionary;

public class generateDictionary {
	
	private static String sDate;
	private static float index;
	private static tempDictionary dictionaryList = new tempDictionary();
	private static ArrayList<String> tempList = new ArrayList<String>();
	private static Calendar date;
	private static String directory_path = "directory path";
	private static Connection conn;

	public static void main(String[] args) {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"db", "id", "password");

				date = Calendar.getInstance();
				date.set(2014, Calendar.JANUARY, 1);
				SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
				System.out.print("[");
				while (!(sDate = dFormat.format(date.getTime()))
						.equals("2014-12-30")) {
					retrieveData();
					date.add(Calendar.DATE, 1);
				}
				insertData();
				dictionaryList.clear();
			conn.close();

		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("[end]");
	}

	private static void insertData() {
		String ouputFileName = "file name";
		BufferedWriter writeArticle;
		try {
			writeArticle = new BufferedWriter(new FileWriter(ouputFileName));
			int size = dictionaryList.sizeOfDictionary();
			for (int i = 0; i < size; i++) {
				writeArticle.write(dictionaryList.getWord(i) + ",");
				writeArticle.write(dictionaryList.getFrequency(i) + ",");
				writeArticle.write(dictionaryList.getPositive(i) + ",");
				writeArticle.newLine();
			}
			writeArticle.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void retrieveData() {
		try {

			Statement stmt = conn.createStatement();
			String sql;
			ResultSet rs;
			// retrieve index
			{
				// set date for index
				Calendar idate = Calendar.getInstance();
				idate.setTime(date.getTime());
				idate.add(Calendar.DATE, 1);

				sql = "select 구매평 긍정 부정";
				rs = stmt.executeQuery(sql);
				if (rs.next()) {
					index = rs.getFloat("index");
				}
				
			}

			// retrieve article
			{
				File dirFile = new File(directory_path +"\\"+ sDate);
				File[] fileList = dirFile.listFiles();
				String tempPath = null;
				String tempFileName;
				BufferedReader in;
				for (File tempFile : fileList) {
					tempPath = tempFile.getParent();
					tempFileName = tempFile.getName();

					in = new BufferedReader(new FileReader(tempPath + "\\"
							+ tempFileName));
					String s;
					tempList.clear();
					while ((s = in.readLine()) != null) {
						if (!(tempList.contains(s))) {
							tempList.add(s);
						}
					}
					in.close();
					for (String word : tempList) {
						addDictionaryList(word);
					}
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void addDictionaryList(String string) {
		int index = dictionaryList.isContain(string);

		if (index == 999999999) {
			// new word
			dictionaryList.addWord(string);
			index = dictionaryList.isContain(string);
			dictionaryList.addFrequency(index);
		} else {
			// exist word
			dictionaryList.addFrequency(index);
		}

		if (index == 1) {
			dictionaryList.addPositive(index);
		}
	}
}
