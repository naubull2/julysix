package soma.sentiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.snu.ids.ha.ma.Eojeol;
import org.snu.ids.ha.ma.MExpression;
import org.snu.ids.ha.ma.MorphemeAnalyzer;
import org.snu.ids.ha.ma.Sentence;

public class morphemeAnalysis {
	private static String sDate;
	private static String tempArticle;
	private static int tempId;
	private static ArrayList<String> tempList = new ArrayList<String>();
	private static Calendar date;
	private static String directory_path = "directory path";
	private static Connection conn;
	private static int COMPANY_ID = 14;

	public static void main(String[] args) {
		date = Calendar.getInstance();
		date.set(2014, Calendar.JANUARY, 1);
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			// Load the driver
			Class.forName("com.mysql.jdbc.Driver");
			// Create the connection
			conn = DriverManager.getConnection(
					"db", "id", "password");
			
			while (!(sDate = dFormat.format(date.getTime()))
					.equals("2014-12-30")) {
				File f = new File(directory_path + sDate);
				f.mkdir();
				System.out
						.println("-------------------------------------------");
				System.out.println("[search date: " + sDate + "]");
				retrieveData();
				date.add(Calendar.DATE, 1);
			}
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		System.out.println("[end]");
	}

	private static void retrieveData() {
		try {
			// Create a Statement class to execute the SQL statement
			Statement stmt = conn.createStatement();
			// Execute the SQL statement and get the results in a ResultSet
			String query = "select ������";
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				tempId = rs.getInt("newsid");
				tempArticle = rs.getString("article");
				morphemeAnalyzer();
			}

			System.out.println("End");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void morphemeAnalyzer() {

		try {
			MorphemeAnalyzer ma = new MorphemeAnalyzer();
			List<MExpression> ret = ma.analyze(tempArticle);

			ret = ma.postProcess(ret);
			ret = ma.leaveJustBest(ret);

			List<Sentence> stl = ma.divideToSentences(ret);

			tempList.clear();
			for (Sentence sentence : stl) {
				for (Eojeol result : sentence) {
					// extract noun
					String[] temp = result.getSmplStr2().replaceAll("[+]", "/")
							.split("/");
					for (int i = 1; i < temp.length; i += 2) {
						if (temp[i].contains("NN") && temp[i - 1].length() != 1) {
							tempList.add(temp[i - 1]);
						}
					}
				}
			}
			// add dictionary list
			insertData();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void insertData() {
		String ouputFileName = directory_path + sDate + "\\" + tempId + ".txt";
		BufferedWriter writeArticle;

		try {
			writeArticle = new BufferedWriter(new FileWriter(ouputFileName));
			int size = tempList.size();
			for (int i = 0; i < size; i++) {
				writeArticle.write(tempList.get(i));
				writeArticle.newLine();
			}
			writeArticle.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
