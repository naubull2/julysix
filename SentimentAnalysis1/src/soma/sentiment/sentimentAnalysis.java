package soma.sentiment;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import soma.sentiment.Dictionary;
import soma.sentiment.tempMorpheme;

public class sentimentAnalysis {
	private static String sDate;
	private static tempMorpheme morphemeList = new tempMorpheme();
	private static ArrayList<String> tempList = new ArrayList<String>();
	private static Calendar date;
	private static String directory_path = "directory path";
	private static Dictionary dictionary;
	private static int companyId = 10;
	private static Connection conn;
	
	public static void main(String[] args) {
		date = Calendar.getInstance();
		date.set(2014, Calendar.JANUARY, 1);
		SimpleDateFormat dFormat = new SimpleDateFormat("yyyy-MM-dd");
		dictionary = new Dictionary();
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(
					"db", "id", "password");
			// 2013-01-01 ~ 2013-12-31
			while (!(sDate = dFormat.format(date.getTime())).equals("2014-12-30")) {
				retrieveData();
				date.add(Calendar.DATE, 1);
			}
			conn.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	private static void retrieveData() {
		try {

			File dirFile = new File(directory_path + sDate);
			File[] fileList = dirFile.listFiles();
			String tempPath = null;
			String tempFileName;
			BufferedReader in;
			for (File tempFile : fileList) {
				tempPath = tempFile.getParent();
				tempFileName = tempFile.getName();

				in = new BufferedReader(new FileReader(tempPath + "\\"
						+ tempFileName));
				String s;
				morphemeList.clear();
				tempList.clear();
				while ((s = in.readLine()) != null) {
					if (!(tempList.contains(s))) {
						tempList.add(s);
					}
				}
				in.close();

				scoring(tempFileName.split("\\.")[0]);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void scoring(String id) {
		try {
			// matching positive_index of word
			for (String s : tempList) {
				int d_index = dictionary.contains(s);
				if (d_index != -1) {
					morphemeList.addWord(s);
					int index = morphemeList.isContain(s);
					float pindex = dictionary.getPositive(d_index);
					morphemeList.setPositive(index, pindex);
				}
			}
			System.out.print("i");
			
			// score positive_index of article
			float pScore = 0;
			int i = 0;
			for (i = 0; i < morphemeList.positive.size(); i++) {
				pScore += morphemeList.getPositive(i);
			}
			if (pScore != 0)
				pScore = pScore / i;

			// insert positive_index of article
			Statement stmt = conn.createStatement();
			String updateQuery = "update positive index";
			int updateCnt = stmt.executeUpdate(updateQuery);
			if (updateCnt == 0)
				System.out.println("[ 'id:" + id + "' is not updated ]");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
