package soma.sentiment;

import java.util.ArrayList;

public class tempDictionary {
	public ArrayList<String> word;
	public ArrayList<Integer> frequency;
	public ArrayList<Integer> positive;

	public int isContain(String inWord) {
		int index = 999999999;

		if (word.contains(inWord))
			index = word.indexOf(inWord);

		return index;
	}

	public int sizeOfDictionary() {
		return word.size();
	}

	public void addWord(String inWord) {
		word.add(inWord);
		frequency.add(0);
		positive.add(0);
	}

	public void addFrequency(int index) {
		frequency.set(index, (frequency.get(index) + 1));
	}

	public void addPositive(int index) {
		positive.set(index, (positive.get(index) + 1));
	}

	public String getWord(int index) {
		return word.get(index);
	}

	public int getFrequency(int index) {
		return frequency.get(index);
	}

	public int getPositive(int index) {
		return positive.get(index);
	}

	public tempDictionary() {
		word = new ArrayList<String>();
		frequency = new ArrayList<Integer>();
		positive = new ArrayList<Integer>();
	}
	
	public void clear(){
		word.clear();
		frequency.clear();
		positive.clear();
	}

}
