package soma.sentiment;

import java.util.ArrayList;

public class tempMorpheme {
	public ArrayList<String> word;
	public ArrayList<Integer> frequency;
	public ArrayList<Float> positive;
	public ArrayList<Float> negative;
	
	public int isContain(String inWord) {
		int index = 999999999;

		if (word.contains(inWord))
			index = word.indexOf(inWord);

		return index;
	}

	public int sizeOfDictionary() {
		return word.size();
	}

	public void addWord(String inWord) {
		word.add(inWord);
		frequency.add(0);
		positive.add((float) 0);
		negative.add((float) 0);
	}

	public void addFrequency(int index) {
		frequency.set(index, (frequency.get(index) + 1));
	}

	public void setPositive(int index, float p_index) {
		positive.set(index, p_index);
	}
	
	public void setNegative(int index, float n_index) {
		negative.set(index, n_index);
	}

	public String getWord(int index) {
		return word.get(index);
	}

	public int getFrequency(int index) {
		return frequency.get(index);
	}

	public float getPositive(int index) {
		return positive.get(index);
	}
	
	public float getNegative(int index) {
		return negative.get(index);
	}

	public void clear() {
		word.clear();
		frequency.clear();
		positive.clear();
		negative.clear();
	}

	public tempMorpheme() {
		word = new ArrayList<String>();
		frequency = new ArrayList<Integer>();
		positive = new ArrayList<Float>();
		negative = new ArrayList<Float>();
	}
}
