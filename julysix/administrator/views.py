from django.shortcuts import render, get_object_or_404, render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.template.response import TemplateResponse
from django.template import RequestContext
from db.models import *
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django import forms
from django.core.exceptions import ObjectDoesNotExist

# Create your views here.
def home(request):
    # ret_products = divideProduct()
    # return TemplateResponse(request, "administrator_module/admin_home.html", {'products0':ret_products[0], 'products1':ret_products[1], 'products2':ret_products[2]})
    return redirect('product_management')

def product_management(request):
    form = ImageForm()
    user = request.user
    products = Product.objects.filter(product_seller_id=user.username)

    return TemplateResponse(request, "administrator_module/admin_product.html", {'products':products, 'form': form})
    # ret_products = divideProduct()
    # return TemplateResponse(request, "administrator_module/admin_product.html", {'products0':ret_products[0], 'products1':ret_products[1], 'products2':ret_products[2], 'form': form})

def shipment_management(request):
    if request.method == 'POST':
        order_id = request.POST.get('order_id')
        delivery_company_id = request.POST.get('delivery_company_id')
        invoice_number = request.POST.get('invoice_number')
        isMod = request.POST.get('isMod')

        if isMod == '1':
            delivery = DeliveryCompany.objects.get(id=delivery_company_id)
            shipment = Shipment.objects.filter(order_id=order_id).update(delivery_company=delivery, invoice_number=invoice_number)
        else:
            shipment = Shipment(order_id=order_id, delivery_company_id=delivery_company_id, invoice_number=invoice_number)
            shipment.save()
        return redirect('shipment_management')

    user = request.user
    products = Product.objects.filter(product_seller_id=user.username)
    deliverys = DeliveryCompany.objects.all()
    orders = []
    for product in products:
        temp=Order.objects.filter(product_id=product.id)
        for order in temp:
            orders.append(order)

    lists = []
    for order in orders:
      try:
        try:
          temp = Shipment.objects.get(order_id=order.id)
        except ObjectDoesNotExist:
          temp = '0'
      finally:
        lists.append((order, temp))

    return TemplateResponse(request, "administrator_module/admin_shipment.html", {'lists':lists, 'deliverys':deliverys})


@login_required
def logged_in(request):
    return HttpResponseRedirect('/')

def login_user(request):
    state = "Please log in below..."
    username = password = ''
    if request.POST:
        username = request.POST.get('login_user_id')
        password = request.POST.get('login_password')

        user = authenticate(username=username,password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                state = "You're successfully logged in !"
            else:
                state = "Your account is not active, please contact the site admin"
        else:
            state = "Your username and/or password were incorrect"

    return HttpResponseRedirect('/administrator/')

def logout_user(request):
    logout(request)
    response = redirect('/')
    response.delete_cookie('user_location')
    return response

class ImageForm(forms.Form):
    imageFile = forms.FileField(
        label = 'Select a file'
            )

def product_upload(request):
    if request.method == 'POST':
        product_name = request.POST.get('product_name')
        product_stock = request.POST.get('product_stock')
        product_price = request.POST.get('product_price')
        product_seller_id = request.POST.get('product_seller_id')
        product_url = request.POST.get('product_url')
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            newProduct = Product()
            newProduct.product_name = product_name
            newProduct.product_stock = product_stock
            newProduct.product_price = product_price
            newProduct.product_image = request.FILES['imageFile']
            newProduct.product_seller_id = product_seller_id
            newProduct.product_url = product_url
            newProduct.save()
            return redirect('product_management')

            # ret_products = divideProduct()
            # return TemplateResponse(request, "administrator_module/admin_product.html", {'products0':ret_products[0], 'products1':ret_products[1], 'products2':ret_products[2], 'form': form})

    else:
        return redirect('product_management')
        # form = ImageForm()

    # ret_products = divideProduct()
    # return TemplateResponse(request, "aadministrator_module/dmin_product.html", {'products0':ret_products[0], 'products1':ret_products[1], 'products2':ret_products[2], 'form': form})

# def divideProduct():
#     products = Product.objects.all()
#     ret_products = []
#     for i in xrange(3):
#         ret_products.append([])
#
#     cnt = 0
#     for product in products:
#         ret_products[cnt].append(product)
#         cnt = cnt + 1
#         if cnt == 3:
#             cnt = 0
#     return ret_products
