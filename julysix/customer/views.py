# _*_ coding: utf-8 _*_

from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.template import Context, RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import logout
from db.models import *
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.forms import UserCreationForm
from django.contrib import auth

def mainPage(request):
  if request.user.is_authenticated():
      if request.user.is_staff:
          return redirect('/administrator/product_management')
      else:
          return redirect('/customer/inquiry/')
  else:
    return redirect('/customer/login/?next=%s' % request.path)

def inquiryPage(request):
  user=request.user
  lists=getLists(user.id, Shipment)

  return render_to_response(
    'customer_module/inquiryPage.html', RequestContext(request, {
      'lists': lists
    })
  )

def canclePage(request):
  if request.method=='POST':
    order = get_object_or_404(Order, id=request.POST['id'])
    order.delete()
    return redirect('canclePage')

  user=request.user
  lists = getLists(user.id, Shipment)

  return render_to_response(
    'customer_module/canclePage.html', RequestContext(request, {'lists':lists})
  )

def reviewPage(request):
  if request.method=='POST':

    if 'comment' in request.POST :
      comment = request.POST['comment']
      orderId = request.POST['id']
      review = Review(order_id=orderId, comment=comment, rate=0)
      review.save()
      return redirect('reviewPage')
    if 'comment2' in request.POST :
      comment2 = request.POST['comment2']
      orderId = request.POST['id']
      review = Review.objects.filter(order_id=orderId).update(comment=comment2)
      return redirect('reviewPage')

  user = request.user
  lists = getLists(user.id, Review)

  return render_to_response(
    'customer_module/reviewPage.html', RequestContext(request, {'lists':lists})
    )

def logoutPage(request):
  logout(request)
  return HttpResponseRedirect('/')

def getLists(userId, secondElement):
  orders = Order.objects.filter(user_id=userId)
  lists = []
  for order in orders:
    try:
      try:
        temp = secondElement.objects.get(order_id=order.id)
      except ObjectDoesNotExist:
        temp = '0'
    finally:
      lists.append((order, temp))

  return lists

def signUp(request):
    user = User.objects.create_user(username=request.POST['username'],
    password=request.POST['password'])
    # print 'create', user
    user = auth.authenticate(username=request.POST['username'], password=request.POST['password'])
    # print 'authenticated', user
    auth.login(request, user)
    return HttpResponseRedirect(request.POST.get('next') or '/')
