from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin


class Product(models.Model):
    product_name=models.CharField(max_length=30)
    product_image=models.FileField(upload_to='images/%Y/%m/%d')
    product_stock=models.PositiveIntegerField()  # needs to be added to db_product
    product_price=models.PositiveIntegerField()  # needs to be added to db_product
    product_seller_id=models.CharField(max_length=30)
    product_url=models.CharField(max_length=100)

class Order(models.Model):
    user = models.ForeignKey(User)
    product = models.ForeignKey(Product)
    date = models.DateTimeField(auto_now_add=True)
    orderer_address = models.CharField(max_length=100)
    orderer_name = models.CharField(max_length=20)
    orderer_phone = models.CharField(max_length=20)

class Seller(models.Model):
    password = models.CharField(max_length=128)
    email = models.CharField(max_length=75)

class Blog(models.Model):
    name = models.CharField(max_length=128)
    url = models.CharField(max_length=128)

class PayModule(models.Model):
    product = models.ForeignKey(User)
    seller = models.ForeignKey(Seller)
    blog = models.ForeignKey(Blog)
    url = models.CharField(max_length=128)

class ChattingUser(models.Model):
    password = models.CharField(max_length=128)
    name = models.CharField(max_length=75)

class ChattingRoom(models.Model):
    product = models.ForeignKey(User)
    chattingUser = models.ForeignKey(ChattingUser)
    contents = models.TextField()

class DeliveryCompany(models.Model):
    name = models.CharField(max_length=30)
    service_url = models.CharField(max_length=300)

class Shipment(models.Model):
    order = models.ForeignKey(Order)
    delivery_company = models.ForeignKey(DeliveryCompany)
    invoice_number = models.CharField(max_length=15)
    isEnd = models.BooleanField(default=False)

class Review(models.Model):
    order = models.ForeignKey(Order)
    comment = models.TextField(blank=False)
    rate = models.IntegerField()
