from django.conf.urls import patterns, include, url

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'julysix.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^administrator/$', 'administrator.views.home', name='home'),
    url(r'^administrator/logged_in/$', 'administrator.views.logged_in'),
    url(r'^administrator/login/$', 'administrator.views.login_user'),
    url(r'^administrator/logout/$', 'administrator.views.logout_user'),
    url(r'^administrator/product_upload/$', 'administrator.views.product_upload', name='product_upload'),
    url(r'^administrator/product_management/$', 'administrator.views.product_management', name='product_management'),
    url(r'^administrator/shipment_management/$', 'administrator.views.shipment_management', name='shipment_management'),

    #customer_module
    url(r'^customer/login/$', 'django.contrib.auth.views.login'),
    url(r'^customer/logout/$','customer.views.logoutPage', name='logoutPage'),
    #page
    url(r'^$', 'customer.views.mainPage', name='customer_home'),
    url(r'^customer/inquiry/$', 'customer.views.inquiryPage', name='inquiryPage'),
    url(r'^customer/cancle/$', 'customer.views.canclePage', name='canclePage'),
    url(r'^customer/review/$', 'customer.views.reviewPage',name='reviewPage'),
    #function

    url(r'^signup/$', 'customer.views.signUp',name='singUp'),


    #purchase_module
    url(r'^purchase/(?P<product_id>\w+)/$', 'purchase.views.purchasePage', name='purchasePage'),

    )
