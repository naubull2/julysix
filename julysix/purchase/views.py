# -*- coding: cp949 -*-

from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.contrib.auth.models import User
from db.models import *
from django.template import Context, RequestContext
from datetime import date, datetime
from django.http import HttpResponse, HttpResponseRedirect


def purchasePage(request, product_id):
    if request.user.is_authenticated():
        product = get_object_or_404(Product, id=product_id)
        message = '0'
        if request.method=='POST':
            user_id=request.user.id
            product_id=request.POST['product_id']
            orderer_name=request.POST['orderer_name']
            orderer_address=request.POST['zipcode1']+'-'+request.POST['zipcode2']+' '+request.POST['address']
            orderer_phone=request.POST['orderer_phone']
            order = Order(user_id=user_id, product_id=product_id, orderer_name=orderer_name, orderer_address=orderer_address, orderer_phone=orderer_phone)
            order.save()
            message='1'
            return render_to_response(
                'purchase_module/purchase.html', RequestContext(request, {
                'product': product,
                'message': message
                })
            )

        return render_to_response(
            'purchase_module/purchase.html', RequestContext(request, {
            'product': product,
            'message': message
            })
        )

    else:
        return redirect('/customer/login/?next=%s' % request.path)
