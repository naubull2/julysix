from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin


class Product(models.Model):
  product_name=models.CharField(max_length=30)
  product_image=models.FileField(upload_to='/images/%Y/%m/%d')

class Order(models.Model):
  user = models.ForeignKey(User)
  product = models.ForeignKey(Product)
  #date = models.DateField(auto_now_add=True)

class DeliveryCompany(models.Model):
  name = models.CharField(max_length=30)
  service_url = models.CharField(max_length=300)

class Shipment(models.Model):
  order = models.ForeignKey(Order)
  delivery_company = models.ForeignKey(DeliveryCompany)
  invoice_number = models.CharField(max_length=15)
  isEnd = models.BooleanField(default=False)

class Review(models.Model):
  order = models.ForeignKey(Order)
  comment = models.TextField(blank=False)
  rate = models.IntegerField()
