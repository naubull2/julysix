from django.conf.urls import patterns, include, url
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from customer.views import *
from customer.models import *
from django.views.generic import DeleteView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'julysix_customer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    #manage session
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$','customer.views.logoutPage', name='logoutPage'),
    #page
    url(r'^$', 'customer.views.mainPage', name='home'),
    url(r'^inquiry/$', 'customer.views.inquiryPage', name='inquiryPage'),
    url(r'^cancle/$', 'customer.views.canclePage', name='canclePage'),
    url(r'^review/$', 'customer.views.reviewPage',name='reviewPage'),
    #function
    #url(r'^cancle/(?P<pk>\d+)$', 'customer.views.cancleOrder', name='cancleOrder'),
)

if settings.DEBUG:
  urlpatterns+= static(settings.STATIC_URL,
                        document_root=settings.STATIC_ROOT)
  urlpatterns+= static(settings.MEDIA_URL,
                        document_root=settings.MEDIA_ROOT)
