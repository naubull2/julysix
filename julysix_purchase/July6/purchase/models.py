# -*- coding: utf-8 -*-
from django.db import models

class Options(models.Model):
    category = models.CharField()
    options = models.TextField() # input with delimiter '/'   
    
class Product(models.Model):
    id = 0
    
    productName = models.CharField(max_length=80)
    productPriceWons = models.IntegerField()
    productPhoto = models.CharField()
    
    timeCreated = models.DateTimeField()
    stock = models.IntegerField()
    # Options is another model
    customOptionList = models.ForeignKey(Options)

