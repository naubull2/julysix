#!/usr/bin/python2.7

import wx
import requests
import json
import time
import re
import threading

REST_SERVER = '127.0.0.1'
REST_PORT = 5000
REST_ROOT = 'rest/top/configurations/running/'
REST_USER = 'admin'
REST_PASSWORD = 'admin'
HEADERS = {}
HEADERS['Content-Type'] = 'application/json'

class StatusManager :
	#########################################
	# Stores all the status of the program as boolean flags.		#
	# Refered from other classes, it helps to make any policy		#
	# or set the label of text box in GUI (alert or sth)			#
	#########################################
	def __init__(self, interface) :
		## Normal flag is for marking the status of attack onto GUI.
		## Three basic cases ; Smurf / Scan / Random
		# Smurf : ICMP Broadcast which is directed to spoofed internal subnet
		# Scan : ICMP Unicast which is directed from internal host to spoofed internal subnet
		# Random : ICMP Unicast which is directed from external random host to spoofed internal subnet
		self.smurf_policy_flag = False				# flag no. 1
		self.scanning_policy_flag = False			# flag no. 2
		self.random_policy_flag = False				# flag no. 3

		# These flags are useful to detect whether it is under attack or not
		# These flags are set to normal (False) every second.
		self.smurf_attack_flag = False				# flag no. 10
		self.scanning_attack_flag = False				# flag no. 20
		self.random_attack_flag = False				# flag no. 30

		## Two basic concepts ; High rate / High flow 
		# High rate : high amout of rate
		# High flow : high number of flows
		self.smurf_high_rate_flag = False			# flag no. 11
		self.smurf_high_flow_flag = False			# flag no. 12
		self.scanning_high_rate_flag = False			# flag no. 21
		self.scanning_high_flow_flag = False			# flag no. 22
		self.random_high_rate_flag = False			# flag no. 31
		self.random_high_flow_flag = False			# flag no. 32

		## These flags are for monitoring ON/OFF
		self.monitor_smurf = False				# flag no. 100
		self.monitor_scanning = False				# flag no. 200
		self.monitor_random = False				# flag no. 300

		self.interface = interface

	def set_flow_counter(self, flow_counter) :
		self.flow_counter = flow_counter

	def set_policy_manager(self, policy_manager) :
		self.policy_manager = policy_manager

	def check_flag(self, number) :
		# returns the value of flags depending on the request type
		if number == 100 : return self.monitor_smurf
		elif number == 200 : return self.monitor_scanning
		elif number == 300 : return self.monitor_random

		elif number == 1 : return self.smurf_policy_flag
		elif number == 2 : return self.scanning_policy_flag
		elif number == 3 : return self.random_policy_flag
		
		elif number == 10 : return self.smurf_attack_flag
		elif number == 20 : return self.scanning_attack_flag
		elif number == 30 : return self.random_attack_flag

		elif number == 11 : return self.smurf_high_rate_flag
		elif number == 12 : return self.smurf_high_flow_flag
		elif number == 21 : return self.scanning_high_rate_flag
		elif number == 22 : return self.scanning_high_flow_flag
		elif number == 31 : return self.random_high_rate_flag
		elif number == 32 : return self.random_high_flow_flag
		else : print "FLAG NUMBER ERROR! __check_flag__"

	def set_flag(self, number) :
		# sets a flag into 'True' depending on the request type
		if number == 1 : self.smurf_policy_flag = True
		elif number == 2 : self.scanning_policy_flag = True
		elif number == 3 : self.random_policy_flag = True
		
		elif number == 10 : self.smurf_attack_flag = True
		elif number == 20 : self.scanning_attack_flag = True
		elif number == 30 : self.random_attack_flag = True

		elif number == 11 : self.smurf_high_rate_flag = True
		#elif number == 12 : self.smurf_high_flow_flag = True
		elif number == 21 : self.scanning_high_rate_flag = True
		elif number == 22 : self.scanning_high_flow_flag = True
		elif number == 31 : self.random_high_rate_flag = True
		elif number == 32 : self.random_high_flow_flag = True

		elif number == 100 : self.monitor_smurf = True
		elif number == 200 : self.monitor_scanning = True
		elif number == 300 : self.monitor_random = True
		else : print "FLAG NUMBER ERROR! __set_flag__"

	def clear_flag(self, number) :
		# sets a flag into 'False' depending on the request type
		if number == 1 : self.smurf_policy_flag = False
		elif number == 2 : self.scanning_policy_flag = False
		elif number == 3 : self.random_policy_flag = False
		
		elif number == 10 : self.smurf_attack_flag = False
		elif number == 20 : self.scanning_attack_flag = False
		elif number == 30 : self.random_attack_flag = False

		elif number == 11 : self.smurf_high_rate_flag = False
		#elif number == 12 : self.smurf_high_flow_flag = False
		elif number == 21 : self.scanning_high_rate_flag = False
		elif number == 22 : self.scanning_high_flow_flag = False
		elif number == 31 : self.random_high_rate_flag = False
		elif number == 32 : self.random_high_flow_flag = False

		elif number == 100 : self.monitor_smurf = False
		elif number == 200 : self.monitor_scanning = False
		elif number == 300 : self.monitor_random = False
		else : print "FLAG NUMBER ERROR! __clear_flag__"

	def monitor_onoff(self, attack_type) :
		# Monitoring ON/OFF switch.
		# First, it checks what attack it is for
		# Second, check if the switch is already on
		# FInally, it changes the status of ON/OFF
		if attack_type == "broadcast" :
			if self.check_flag(100) :
				self.clear_flag(100)
				print "Broadcast monitoring OFF"
			else :
				self.set_flag(100)
				print "Broadcast monitoring ON"
			return
		elif attack_type == "scanning" :
			if self.check_flag(200) :
				self.clear_flag(200)
				print "Scanning monitoring OFF"
			else :
				self.set_flag(200)
				print "Scanning monitoring ON"
			return
		elif attack_type == "random" :
			if self.check_flag(300) :
				self.clear_flag(300)
				print "Random monitoring OFF"
			else :
				self.set_flag(300)
				print "Random monitoring ON"
			return

	def policy_clear(self, attack_type) :
		# Status manager directly order policy_manager to clear the policies
		# Status manager directly order flow_counter to clear the subnet lists / dicts		
		if attack_type == "broadcast" :
			self.policy_manager.broadcast_clear()
		elif attack_type == "scanning" :
			self.policy_manager.scanning_clear()
		elif attack_type == "random" :
			self.policy_manager.random_clear()

class FlowChecker :
	#########################################
	# The main checking module.							#
	# STEP 1 : using check_flow(), it prepares to check flows.		#
	# STEP 2 : In check_flow(), it starts to check the flows using	#
	# Threads with FlowCounter.							#
	#	2.1 The first thread : broadcast attack checking			#
	#	2.2 The second thread : scanning attack checking		#
	#	2.3 The third thread : rand-source attack checking		#
	# STEP 3 : If any attack is checked by FlowCounter, it reports to #
	# StatusManager.									#
	#########################################
	def __init__(self, interface) :
		self.interface = interface
		self.basic_url = ""
		self.request_name_url = ""			# url for requesting names of current flows
		self.broadcast_subnet = ""			# store the subnet for making policy in pm

	def set_status_manager(self, status_manager) :
		self.status_manager = status_manager

	def set_flow_counter(self, flow_counter) :		# share the counter connected with Monitor
		self.flow_counter = flow_counter

	def printKeyVals(self, value, indent=0):
		""" used for neatly showing json data """
		if isinstance(value, list):
		    print
		    for item in value:
		        self.printKeyVals(item, indent+1)
		elif isinstance(value, dict):
		    print
		    for k, v in value.iteritems():
		        print "    " * indent, k + ":",
		        self.printKeyVals(v, indent + 1)
		else:
		    print value

	def check_broadcast_flow(self, jdata) :
		# This method is for checking broadcast attack.
		# It is called as a thread.
		# in broadcast attack case, we don't have to care about the number of flows.
		# It is because ICMP has no port. To do attack, the attacker has nothing to do but
		# increasing flow rate of only one flow.
		is_highest = True
		for i in range(len(jdata[u'collection'])) :
			flow_description = jdata[u'collection'][i][u'description']
			flow_name = jdata[u'collection'][i][u'name']
			flow_rate = float(jdata[u'collection'][i][u'future_rate'])
			# If protocol == icmp implemented, this silly process should be gone!!			
			for j in reversed(range(len(flow_description))) :				 
				if flow_description[j] == 'I' : ## this flow is ICMP
					IPaddress = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", flow_description)
					sourceIP = re.split("\.", IPaddress[0])
					destinationIP = re.split("\.", IPaddress[1])
					if (sourceIP[0:3] == destinationIP[0:3]) & (destinationIP[3] == '255') :		# 1. suspicious
					# ICMP x.x.A.x => x.x.A.255
					# If ICMP broadcast is found, it checks the rate of the highest flow first.
						if is_highest :
							if flow_rate >= 20.0 : ## The highest ICMP broadcast rate is over 20
								self.status_manager.set_flag(11)						
							is_highest = False

						if self.status_manager.check_flag(11) :							# 2. high rate
							self.status_manager.set_flag(10)							# 1&2 -> It's under attack
							if self.flow_counter.check_broadcast_subnet(destinationIP) == False :	# 3. no policy yet
								self.flow_counter.add_broadcast_subnet(destinationIP)		# 1&2&3 -> add subnet and..
								self.flow_counter.police_broadcast(destinationIP)			# request flowcounter to police
					break
			if i >= 999 : break
		if self.flow_counter.get_broadcast_subnet_length() > 0 : self.status_manager.set_flag(1)

	def check_scanning_flow(self, jdata) :
		# This method is for checking ICMP scanning attack.
		# It is called as a thread.
		# it compares the source and destination subnet until 24bits.
		# and by checking the highest rate and counts of flows (more than 20), it requests flow counter to make a policy
		is_highest = True
		for i in range(len(jdata[u'collection'])) :
			flow_description = jdata[u'collection'][i][u'description']
			flow_name = jdata[u'collection'][i][u'name']
			flow_rate = float(jdata[u'collection'][i][u'future_rate'])
			# If protocol == icmp implemented, this silly process should be gone!!			
			for j in reversed(range(len(flow_description))) :				 
				if flow_description[j] == 'I' : ## this flow is ICMP
					IPaddress = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", flow_description)
					sourceIP = re.split("\.", IPaddress[0])
					destinationIP = re.split("\.", IPaddress[1])

					if (sourceIP[0:3]==destinationIP[0:3]) & (destinationIP[3] != '255') :				# 1. suspicious
					# ICMP A.B.C.x => A.B.C.x
						# If ICMP scanning attack is found, it checks the rate of the highest flow first.
						if is_highest :
							if flow_rate >= 20.0 : ## The highest ICMP scanning rate is over 20
								self.status_manager.set_flag(21)
							is_highest = False
						if self.flow_counter.count_scanning_subnet(destinationIP[0:3]) : self.status_manager.set_flag(22)
						if self.status_manager.check_flag(21) | self.status_manager.check_flag(22) : 	# 2. high rate / high flow
							self.status_manager.set_flag(20)								# 1&2 -> It's under attack
							if self.flow_counter.check_scanning_subnet(destinationIP) == False :		# 3. no policy yet
								self.flow_counter.add_scanning_subnet(destinationIP)				# 1&2&3 -> add subnet and..
								self.flow_counter.police_scanning(destinationIP)					# request flowcounter to police
					break
			if i >= 999 : break
		if self.flow_counter.get_scanning_subnet_length() > 0 : self.status_manager.set_flag(2)

	def check_random_flow(self, jdata) :
		# This method is for checking ICMP random attack.
		# It is called as a thread.
		# it skips iteration if the source and destination is in the same subnet until 24bits.
		# and by checking the highest rate and counts of flows(more than 100), it requests flow counter to make a policy
		is_highest = True
		print "Random checking"
		for i in range(len(jdata[u'collection'])) :
			flow_description = jdata[u'collection'][i][u'description']
			flow_name = jdata[u'collection'][i][u'name']
			flow_rate = float(jdata[u'collection'][i][u'future_rate'])
			# If protocol == icmp implemented, this silly process should be gone!!			
			for j in reversed(range(len(flow_description))) :				 
				if flow_description[j] == 'I' : ## this flow is ICMP
					IPaddress = re.findall("\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}", flow_description)
					sourceIP = re.split("\.", IPaddress[0])
					destinationIP = re.split("\.", IPaddress[1])

					if (sourceIP[0:3]!=destinationIP[0:3]) :									# 1. suspicious (rand-source ICMP)
					# ICMP A.B.C.x => A.B.C.x
						# If ICMP random attack is found, it checks the rate of the highest flow first.
						if is_highest :
							if flow_rate >= 20.0 : ## The highest ICMP random rate is over 20
								self.status_manager.set_flag(31)
							is_highest = False
						if self.flow_counter.count_random_subnet(destinationIP[0:3]) : self.status_manager.set_flag(32)
						if self.status_manager.check_flag(31) | self.status_manager.check_flag(32) : 	# 2. high rate / high flow
							self.status_manager.set_flag(30)								# 1&2 -> It's under attack
							if self.flow_counter.check_random_subnet(destinationIP) == False :		# 3. no policy yet
								self.flow_counter.add_random_subnet(destinationIP)				# 1&2&3 -> add subnet and..
								self.flow_counter.police_random(destinationIP)					# request flowcounter to police
					break
			if i >= 999 : break
		if self.flow_counter.get_random_subnet_length() > 0 : self.status_manager.set_flag(3)

	def check_flow(self):
		## Check_Flow
		# STEP1 : when called, it gets upto 1000 flows of current flows on eth1 utilizing GET
		# STEP2 : Sending the json data to each checking thread (broadcast, scanning, random) as
		#		parameter, it checks all the attacks simultaneosly.
		# ** From the test that I had done before, the program endures checking about 4000 flows
		# at one second. Since these processes(3 threads * 1000) handle upto 3000 flows, it
		# doesn't make any matter.
		##

		# gets upto ICMP 1000 flows through eth1
		url = self.make_url(REST_ROOT+"interfaces/"+self.interface+"/flows?select=name,description,future_rate&order=<future_rate") # make an url for searching every name of flow
		jdata = self.get_url(url).json()		
		## Sets the ICMP counts every second
		self.flow_counter.set_icmp_flow_counts(jdata[u'size'])
		if int(jdata[u'size']) == 0 : return
		## Sets the highest ICMP flow rate into flow_count every second 
		highest_icmp_flow_name = jdata[u'collection'][0][u'name']
		highest_icmp_flow_rate = jdata[u'collection'][0][u'future_rate']
		self.flow_counter.add_highest_icmp_flow(highest_icmp_flow_name, highest_icmp_flow_rate)
		## flow scanning threads
		t1 = threading.Thread(target = self.check_broadcast_flow, args=(jdata,))
		t2 = threading.Thread(target = self.check_scanning_flow, args=(jdata,))
		t3 = threading.Thread(target = self.check_random_flow, args=(jdata,))
		if self.status_manager.check_flag(100) : t1.start()
		if self.status_manager.check_flag(200) : t2.start()
		if self.status_manager.check_flag(300) : t3.start()

	def make_url(self, suffix):
		# Making url for Rest API. it returns basic format of url with suffix
		return "http://%s:%d/%s" % (REST_SERVER, REST_PORT, suffix)

	def get_url(self, request_url):
		# Retrieves information of flows using GET and returns of the response
		response = requests.get(request_url, auth=(REST_USER, REST_PASSWORD))
		return response

class FlowCounter :
	def __init__(self, interface) :
		self.interface = interface
		##### Storage for general ICMP flow information #####
		self.highest_ICMP_flow_dictionary={}
		self.highest_ICMP_flow_name = ""
		self.ICMP_flow_counts = 0

		##### Storage for ICMP Broadcast Attack information #####
		self.broadcast_subnet_list = []

		##### Storage for ICMP Scanning Attack information #####
		# Stores the subnet which is already applied through REST API
		self.scanning_subnet_list = []
		# Stores ICMP flows in the form of { source_IP/dest_IP : count }
		self.scanning_ICMP_dictionary = {}
		self.scanning_ICMP_count = 0

		##### Storage for ICMP Random Attack information #####
		# Stores the subnet which is already applied through REST API
		self.random_subnet_list = []
		# Stores ICMP flows in the form of { source_IP/dest_IP : count }
		self.random_ICMP_dictionary = {}
		self.random_ICMP_count = 0

	def set_policy_manager(self, policy_manager) :
		self.policy_manager = policy_manager

	################ For ICMP Broadcast Attack ################
	def check_broadcast_subnet(self, broadcast_subnet) :
		# checks if any policy of specific subnet is already applied.
		for i in range(len(self.broadcast_subnet_list)) :
			if self.broadcast_subnet_list[i] == broadcast_subnet : return True
		return False

	def add_broadcast_subnet(self, broadcast_subnet) :
		# This method is for adding the subnet which will be applied by blocking policy to the list.
		self.broadcast_subnet_list.append(broadcast_subnet)

	def get_broadcast_subnet_length(self) :
		return len(self.broadcast_subnet_list)

	def police_broadcast(self, broadcast_subnet) :
		# It requests PolicyMaker to make a blocking policy of ICMP broadcast
		self.policy_manager.broadcast_block(broadcast_subnet)

	def flush_broadcast_subnet(self) :
		self.broadcast_subnet_list = []

	################ For ICMP Scanning Attack ################
	def count_scanning_subnet(self, scanning_subnet) :
		# This method is for adding the subnet to count how many times the specific subnet is
		# targeted.
		# First search if the key is already in dictionary.
		# If exist, it adds the number + 1.
		# At the same time, if the count of subnet is more than 20, it returns true to set high_flow flag
		try :
			self.scanning_ICMP_count = int(self.scanning_ICMP_dictionary[str(scanning_subnet)])
			self.scanning_ICMP_dictionary[str(scanning_subnet)] = self.scanning_ICMP_count + 1
			if self.scanning_ICMP_count == 20 : return True
		except :
			self.scanning_ICMP_dictionary[str(scanning_subnet)] = 1

	def check_scanning_subnet(self, scanning_subnet) :
		# checks if any policy of specific subnet is already applied.
		for i in range(len(self.scanning_subnet_list)) :
			if self.scanning_subnet_list[i] == scanning_subnet : return True
		return False

	def add_scanning_subnet(self, scanning_subnet) :
		# This method is for adding the subnet which will be applied by rate limiting policy to the list.
		self.scanning_subnet_list.append(scanning_subnet)

	def get_scanning_subnet_length(self) :
		return len(self.scanning_subnet_list)

	def police_scanning(self, scanning_subnet) :
		# It requests PolicyMaker to make a rate limiting policy of ICMP scanning attack
		self.policy_manager.scanning_limit(scanning_subnet)

	def flush_scanning_subnet(self) :
		self.scanning_subnet_list = []
		self.scanning_ICMP_dictionary = {}

	################ For ICMP Random Attack ################
	def count_random_subnet(self, random_subnet) :
		# This method is for adding the subnet which is applied ramdom attack defense policy to the list
		# First search fi the key is already in dictionary.
		# If exist, it adds the number + 1.
		# At the same time, if the count of subnet is more than 20, it returns true to set high_flow flag
		try :
			self.random_ICMP_count = int(self.random_ICMP_dictionary[str(random_subnet)])
			self.random_ICMP_dictionary[str(random_subnet)] = self.random_ICMP_count + 1
			if self.random_ICMP_count == 20 : return True
		except :
			self.random_ICMP_dictionary[str(random_subnet)] = 1

	def check_random_subnet(self, random_subnet) :
		# checks if any policy of specific subnet is already applied.
		for i in range(len(self.random_subnet_list)) :
			if self.random_subnet_list[i] == random_subnet : return True
		return False

	def add_random_subnet(self, random_subnet) :
		# This method is for adding the subnet which will be applied by rate limiting policy to the list.
		self.random_subnet_list.append(random_subnet)

	def get_random_subnet_length(self) :
		return len(self.random_subnet_list)

	def police_random(self, random_subnet) :
		# It requests PolicyMaker to make a rate limiting policy of ICMP scanning attack
		self.policy_manager.random_block(random_subnet)

	def flush_random_subnet(self) :
		self.random_subnet_list = []
		self.random_ICMP_dictionary = {}

	##### Method for representing onto GUI #####
	def add_highest_icmp_flow(self, flow_name, rate) :
		self.highest_ICMP_flow_name = flow_name
		self.highest_ICMP_flow_dictionary[flow_name] = rate

	def get_highest_icmp_flow(self):
		if len(self.highest_ICMP_flow_dictionary) == 0 : return "None"
		return self.highest_ICMP_flow_dictionary[self.highest_ICMP_flow_name]

	def set_icmp_flow_counts(self, counts):
		#This method is for setting current counts of ICMP flows into ICMP_flow_counts
		self.ICMP_flow_counts = counts

	def get_icmp_flow_counts(self):
		return self.ICMP_flow_counts

class PolicyManager :
	#########################################
	# The policy making / clearing module using REST API.		#
	# Depending on the request of policy, it constructs / clears	#
	# proper blocking or rate limiting policy of each attack		#
	# It referes the status manager to decide the exact status, and	#
	# is called by FLowCounter. If FlowCounter calculates the attack, #
	# it requests PolicyMaker to make a policy.				#
	#########################################
	def __init__(self, interface) :
		## Caustion !! This program includes Incomplete sequence number
		# In this program, the basic sequence number is 90.
		# The reason is the default sequence number is 100 in configuration of stm.
		# To make a priority, the basic sequence number is lower than 100
		# and every time it makes a new entry in acl or poliscy in ipm, it reduces number 1
		# to make priorities among entries and policies
		# so if entries or policies are more than 90, this program gets into the error. 
		# the configuration is upto devisor. FEEL FREE TO CHANGE THE NUMBER!!
		# and NOTE THAT YOU SHOULD CHANGE CONFIGURATION IN ADVANCE!!
		##
		self.acl_sequence_number = 90
		self.ipm_sequence_number = 90
		self.interface = interface

	def set_status_manager(self, status_manager) :
		self.status_manager = status_manager

	def set_flow_counter(self, flow_counter) :
		self.flow_counter = flow_counter

	def broadcast_block(self, subnet):
		## About broadcast blocking policy
		# STEP 1 : It creates a new acl for ICMP broadcast policies if it doesn't exist.
		# STEP 2 : If the subnet under attack is found, it creates corresponding entries
		#		and the range of the entry is X.X.X.255/32.
		#		This step can be happen when the new subnet under attack is found.
		# STEP 3 : It creates a new ifc for ICMP broadcast policies if it doesn't exist
		# STEP 4 : It creates a new ipm for ICMP broadcast policies if it doesn't exist
		##
		print "==========BROADCAST BLOCK POLICY BUILD=========="
		print "::: Blocking subnet : "+str(subnet[0])+"."+str(subnet[1])+"."+str(subnet[2])+".255 :::"
		print "::: Interface type : "+self.interface + " :::"

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"

		#acl making
		acl_name = self.interface + "_broadcast"
		r = self.post_url(self.make_url("acls/"), { 'name' : acl_name })
		if r.status_code == 201 : print "POST SUCCESS :: acls/" + acl_name + " ==> created."
		#acl entry making
		entry_name = "acl_" + str(subnet[2]) + "_broadcast"
		r = self.post_url(self.make_url("acls/"+acl_name+"/entries/"), { 'name' : entry_name, 'dest_subnet' : str(subnet[0])+'.'+str(subnet[1])+'.'+str(subnet[2])+'.255/32', 'sequence' : self.acl_sequence_number, 'protocol_number' : 1 })
		self.acl_sequence_number = self.acl_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: acls/"+acl_name+"/entries/"+entry_name+" ==> created."
		# ifc making
		ifc_name = "ifc_" + self.interface + "_broadcast"
		r = self.post_url(self.make_url("ifcs/"), {'name' : ifc_name, 'acl' : acl_name, 'egress_flow_class' : efc})
		if r.status_code == 201 : print "POST SUCCESS :: ifcs/"+ifc_name+" ==> created."
		#ipm making
		ipm_name = "ipm_" + self.interface + "_broadcast"
		r = self.post_url(self.make_url("ipms/"+ipm+"/policies/"), {'name' : ipm_name, 'sequence' : self.ipm_sequence_number, 'ifc' : ifc_name, 'drop' : 'T'})
		self.ipm_sequence_number = self.ipm_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> created."
		print "==========BROADCAST BLOCK POLICY BUILD=========="

	def scanning_limit(self, subnet):
		## About scanning attack rate limiting policy
		# STEP 1 : It creates a new acl for ICMP scanning defense policies if it doesn't exist.
		# STEP 2 : If the subnet under attack is found, it creates corresponding entries
		#		and the range of the entry is X.X.X.0/24.
		#		This step can be happen when the new subnet under attack is found.
		# STEP 3 : It creates a new ifc for ICMP scanning defense policies if it doesn't exist
		# STEP 4 : It creates a new ipm for ICMP scanning defense policies if it doesn't exist
		##

		print "==========SCANNING ATTACK POLICY BUILD=========="
		print "::: Limiting subnet : " + str(subnet[0])+"."+str(subnet[1])+"."+str(subnet[2])+"."+str(subnet[3])+" :::"

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"

		#acl making
		acl_name = self.interface + "_scanning"
		r = self.post_url(self.make_url("acls/"), { 'name' : acl_name })
		if r.status_code == 201 : print "POST SUCCESS :: acls/" + acl_name + " ==> created."
		#acl entry making
		entry_name = "acl_" + str(subnet[2]) + "_scanning"
		r = self.post_url(self.make_url("acls/"+acl_name+"/entries/"), { 'name' : entry_name, 'dest_subnet' : str(subnet[0])+'.'+str(subnet[1])+'.'+str(subnet[2])+'.0/24', 'sequence' : self.acl_sequence_number, 'protocol_number' : 1 })
		self.acl_sequence_number = self.acl_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: acls/"+acl_name+"/entries/"+entry_name+" ==> created."
		#ifc making
		ifc_name = "ifc_" + self.interface + "_scanning"
		r = self.post_url(self.make_url("ifcs/"), {'name' : ifc_name, 'acl' : acl_name, 'egress_flow_class' : efc})
		if r.status_code == 201 : print "POST SUCCESS :: ifcs/"+ifc_name+" ==> created."
		#ipm making
		ipm_name = "ipm_" + self.interface + "_scanning"
		r = self.post_url(self.make_url("ipms/"+ipm+"/policies/"), {'name' : ipm_name, 'fixed_rate' : 20, 'sequence' : self.ipm_sequence_number, 'ifc' : ifc_name})
		self.ipm_sequence_number = self.ipm_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> created."
		print "==========SCANNING ATTACK POLICY BUILD=========="

	def random_block(self, subnet):
		## About random blocking policy
		# STEP 1 : It creates a new acl for ICMP random policies if it doesn't exist.
		# STEP 2 : If the host under attack is found, it creates corresponding entries
		#		and the range of the entry is X.X.X.X/32.
		#		** protects specific host
		#		This step can be happen when the new subnet under attack is found.
		# STEP 3 : It creates a new ifc for ICMP broadcast policies if it doesn't exist
		# STEP 4 : It creates a new ipm for ICMP broadcast policies if it doesn't exist
		##

		print "==========RANDOM ATTACK POLICY BUILD=========="
		print "::: Limiting subnet : " + str(subnet[0])+"."+str(subnet[1])+"."+str(subnet[2])+".X :::"

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"
			
		acl_name = self.interface + "_random"
		r = self.post_url(self.make_url("acls/"), { 'name' : acl_name })
		if r.status_code == 201 : print "POST SUCCESS :: acls/" + acl_name + " ==> created."

		entry_name = "acl_" + str(subnet[2]) + "." + str(subnet[3]) + "_random"
		r = self.post_url(self.make_url("acls/"+acl_name+"/entries/"), { 'name' : entry_name, 'dest_subnet' : str(subnet[0])+'.'+str(subnet[1])+'.'+str(subnet[2])+'.'+str(subnet[3])+'/32', 'sequence' : self.acl_sequence_number, 'protocol_number' : 1 })
		self.acl_sequence_number = self.acl_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: acls/"+acl_name+"/entries/"+entry_name+" ==> created."

		ifc_name = "ifc_" + self.interface + "_random"
		r = self.post_url(self.make_url("ifcs/"), {'name' : ifc_name, 'acl' : acl_name, 'egress_flow_class' : efc})
		if r.status_code == 201 : print "POST SUCCESS :: ifcs/"+ifc_name+" ==> created."

		ipm_name = "ipm_" + self.interface + "_random"
		r = self.post_url(self.make_url("ipms/"+ipm+"/policies/"), {'name' : ipm_name, 'drop' : 'T', 'sequence' : self.ipm_sequence_number, 'ifc' : ifc_name})
		self.ipm_sequence_number = self.ipm_sequence_number - 1
		if r.status_code == 201 : print "POST SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> created."
		print "==========RANDOM ATTACK POLICY BUILD=========="

	def broadcast_clear(self):
		## Delete ipm, ifc, acl consequently.
		print "==========BROADCAST BLOCK POLICY CLEAR=========="

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"

		ipm_name = "ipm_" + self.interface + "_broadcast"
		r = self.delete_url(self.make_url("ipms/"+ipm+"/policies/"+ipm_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> deleted."

		ifc_name = "ifc_" + self.interface + "_broadcast"
		r = self.delete_url(self.make_url("ifcs/"+ifc_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ifcs/"+ifc_name+" ==> deleted."

		acl_name = self.interface + "_broadcast"
		r = self.delete_url(self.make_url("acls/"+acl_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: acls/" + acl_name + " ==> deleted."

		self.status_manager.clear_flag(1) 					# set the policy flag to False
		self.flow_counter.flush_broadcast_subnet()				# flush broadcast list in flow_counter
		print "==========BROADCAST BLOCK POLICY CLEAR=========="

	def scanning_clear(self):
		## Delete ipm, ifc, acl consequently.
		print "==========SCANNING ATTACK POLICY CLEAR=========="

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"

		ipm_name = "ipm_" + self.interface + "_scanning"
		r = self.delete_url(self.make_url("ipms/"+ipm+"/policies/"+ipm_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> deleted."

		ifc_name = "ifc_" + self.interface + "_scanning"
		r = self.delete_url(self.make_url("ifcs/"+ifc_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ifcs/"+ifc_name+" ==> deleted."

		acl_name = self.interface + "_scanning"
		r = self.delete_url(self.make_url("acls/"+acl_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: acls/" + acl_name + " ==> deleted."

		self.status_manager.clear_flag(2)
		self.flow_counter.flush_scanning_subnet()
		print "==========SCANNING ATTACK POLICY CLEAR=========="

	def random_clear(self):
		## Delete ipm, ifc, acl consequently.
		print "==========SCANNING ATTACK POLICY CLEAR=========="

		if self.interface == "eth1" : # eth1
			efc = "efc2"
			ipm = "ipm1"
			epm = "epm1"
		else :  # eth2
			efc = "efc1"
			ipm = "ipm2"
			epm = "epm2"

		ipm_name = "ipm_" + self.interface + "_random"
		r = self.delete_url(self.make_url("ipms/"+ipm+"/policies/"+ipm_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ipms/"+ipm+"/policies/"+ipm_name+" ==> deleted."

		ifc_name = "ifc_" + self.interface + "_random"
		r = self.delete_url(self.make_url("ifcs/"+ifc_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: ifcs/"+ifc_name+" ==> deleted."

		acl_name = self.interface + "_random"
		r = self.delete_url(self.make_url("acls/"+acl_name))
		if r.status_code == 200 : print "DELETE SUCCESS :: acls/" + acl_name + " ==> deleted."

		self.status_manager.clear_flag(3)
		self.flow_counter.flush_random_subnet()
		print "==========SCANNING ATTACK POLICY CLEAR=========="

	def make_url(self, suffix):
		return "http://%s:%d/%s%s" % (REST_SERVER, REST_PORT, REST_ROOT, suffix)

	def post_url(self, request_url, dump_data):
		post_response = requests.post(request_url, data=json.dumps(dump_data), headers=HEADERS, auth=(REST_USER, REST_PASSWORD))
		return post_response

	def put_url(self, request_url, dump_data):
		put_response = requests.put(request_url, data=json.dumps(dump_data), headers=HEADERS, auth=(REST_USER, REST_PASSWORD))
		return put_response

	def delete_url(self, request_url):
		delete_response = requests.delete(request_url, auth=(REST_USER, REST_PASSWORD))
		return delete_response

class PublicMessageBox(wx.Panel) :
	#########################################
	# This box is located on the upper side of GUI window.		#
	# Shows public messages such as Highest ICMP rate, ICMP flow	#
	# numbers per second.								#
	#########################################
	def __init__(self, parent, ID):
		wx.Panel.__init__(self, parent, ID)
		self.text_message_rate = wx.StaticText(self, -1, "Highest ICMP rate")
		self.text_eth1_hi_icmp_rate = wx.StaticText(self, -1, "Loading...") 
		self.text_message_number = wx.StaticText(self, -1, "ICMP flow numbers")
		self.text_eth1_icmp_flow_number = wx.StaticText(self, -1, "Loading...")
		
		self.box_hi_rate = wx.BoxSizer(wx.VERTICAL)
		self.box_hi_rate.Add(self.text_message_rate, proportion=0, flag=wx.ALL | wx.ALIGN_CENTER, border=5)
		self.box_hi_rate.Add(self.text_eth1_hi_icmp_rate, proportion=0, flag=wx.ALL | wx.ALIGN_CENTER, border=5)

		self.box_icmp_number = wx.BoxSizer(wx.VERTICAL)
		self.box_icmp_number.Add(self.text_message_number, proportion=0, flag=wx.ALL | wx.ALIGN_CENTER, border=5)
		self.box_icmp_number.Add(self.text_eth1_icmp_flow_number, proportion=0, flag=wx.ALL | wx.ALIGN_CENTER, border=5)

		self.box_public_text = wx.BoxSizer(wx.HORIZONTAL) ## box Including two vertical boxes
		self.box_public_text.Add(self.box_hi_rate, proportion=1, flag=wx.ALL | wx.ALIGN_CENTER, border=10)
		self.box_public_text.AddSpacer(30)
		self.box_public_text.Add(self.box_icmp_number, proportion=1, flag=wx.ALL | wx.ALIGN_CENTER, border=0)

		self.SetSizer(self.box_public_text)
		self.box_public_text.Fit(self)

	def set_eth1_rate_text(self, rate_value) :
		self.text_eth1_hi_icmp_rate.SetLabel(rate_value)

	def set_eth1_number_text(self, num_value) :
		self.text_eth1_icmp_flow_number.SetLabel(num_value)

class AttackControlBox(wx.Panel) :
	#########################################
	# This box is created triple times for each attack(Broadcast, 	#
	# Scan, Random).									#
	# Each box has 3 status texts which indicate				#
	# 1. If it is monitoring or not							#
	# 2. If it is under attack now							#
	# 3. If the policy is constructed now						#
	# each and also two buttons which indicate				#
	# 1. A button calling a monitoring ON/OFF switch 			#
	# of status_manager									#
	# 2. Another button calling policy clearing fuction of			#
	# status_manager									#
	#########################################
	def __init__(self, parent, ID, status_manager, policy_manager, label):
		wx.Panel.__init__(self, parent, ID)
		self.attack_type = label
		self.status_manager = status_manager
		self.policy_manager = policy_manager
		self.text_eth1_monitor = wx.StaticText(self, -1, "Monitor :")
		self.text_eth1_monitor_status = wx.StaticText(self, -1, "")
		self.text_eth1_policy = wx.StaticText(self, -1, "Policy :")
		self.text_eth1_policy_status = wx.StaticText(self, -1, "")
		self.text_eth1_attack = wx.StaticText(self, -1, "Attack :")
		self.text_eth1_attack_status = wx.StaticText(self, -1, "")
		self.box_h1 = wx.BoxSizer(wx.HORIZONTAL)		# Horizontal box for monitor text
		self.box_h2 = wx.BoxSizer(wx.HORIZONTAL)		# Horizontal box for policy text
		self.box_h3 = wx.BoxSizer(wx.HORIZONTAL)		# Horizontal box for attack text
		self.box_h4 = wx.BoxSizer(wx.HORIZONTAL)		# Horizontal box for buttons
		self.box_v1 = wx.StaticBox(self, -1, label)			# Vertical box for all the horizontal boxes
		self.sizer = wx.StaticBoxSizer(self.box_v1, wx.VERTICAL)
		self.btn_eth1_monitor = wx.Button(self, -1, "ON/OFF")
		self.Bind(wx.EVT_BUTTON, self.on_btn_eth1_monitor, self.btn_eth1_monitor)
		self.btn_eth1_clear = wx.Button(self, -1, "Clear")
		self.Bind(wx.EVT_BUTTON, self.on_btn_eth1_clear, self.btn_eth1_clear)
		self.box_h1.Add(self.text_eth1_monitor, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h1.AddSpacer(15)
		self.box_h1.Add(self.text_eth1_monitor_status, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h2.Add(self.text_eth1_policy, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h2.AddSpacer(15)
		self.box_h2.Add(self.text_eth1_policy_status, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h3.Add(self.text_eth1_attack, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h3.AddSpacer(15)
		self.box_h3.Add(self.text_eth1_attack_status, proportion = 1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.box_h4.Add(self.btn_eth1_monitor, proportion=1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=0)
		self.box_h4.AddSpacer(15)
		self.box_h4.Add(self.btn_eth1_clear, proportion=1, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, border=0)
		self.sizer.Add(self.box_h1, proportion=1, flag=wx.TOP | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.sizer.Add(self.box_h2, proportion=1, flag=wx.TOP | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.sizer.Add(self.box_h3, proportion=1, flag=wx.TOP | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.sizer.Add(self.box_h4, proportion=1, flag=wx.TOP | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		self.SetSizer(self.sizer)
		self.sizer.Fit(self)

	def set_eth1_monitor_text(self, text) :
		self.text_eth1_monitor_status.SetLabel(text)

	def set_eth1_policy_text(self, text) :
		self.text_eth1_policy_status.SetLabel(text)

	def set_eth1_attack_text(self, text) :
		self.text_eth1_attack_status.SetLabel(text)

	def on_btn_eth1_monitor(self, event) :
		self.status_manager.monitor_onoff(self.attack_type)

	def on_btn_eth1_clear(self, event) :
		self.status_manager.policy_clear(self.attack_type)

class Frame(wx.Frame) : ## GUI CLASS
	#########################################
	# This class is main frame of program. It structs the GUI of the	#
	# entire program and set the timer to check flows every second.#
	#########################################
	title = 'IDS / IPM Testbed'

	def __init__(self):
		wx.Frame.__init__(self, None, -1, self.title)
		self.eth1_status_manager = StatusManager("eth1")
		self.eth1_flow_checker = FlowChecker("eth1")
		self.eth1_flow_counter = FlowCounter("eth1")
		self.eth1_policy_manager = PolicyManager("eth1")
		self.eth1_status_manager.set_policy_manager(self.eth1_policy_manager)
		self.eth1_status_manager.set_flow_counter(self.eth1_flow_counter)
		self.eth1_flow_checker.set_status_manager(self.eth1_status_manager)
		self.eth1_flow_checker.set_flow_counter(self.eth1_flow_counter)
		self.eth1_flow_counter.set_policy_manager(self.eth1_policy_manager)
		self.eth1_policy_manager.set_status_manager(self.eth1_status_manager)
		self.eth1_policy_manager.set_flow_counter(self.eth1_flow_counter)
		self.create_main_panel()
		# Realtime detection with timer
		self.timer = wx.Timer(self)
		self.Bind(wx.EVT_TIMER, self.on_timer, self.timer)  
		self.timer.Start(1000) # per 1 sec

	def create_main_panel(self):
		# construct the frame of GUI panels
		self.panel = wx.Panel(self)
		self.public_message_box = PublicMessageBox(self.panel, -1)
		self.broadcast_control_box = AttackControlBox(self.panel, -1, self.eth1_status_manager, self.eth1_policy_manager, "broadcast")
		self.scanning_control_box = AttackControlBox(self.panel, -1, self.eth1_status_manager, self.eth1_policy_manager, "scanning")
		self.random_control_box = AttackControlBox(self.panel, -1, self.eth1_status_manager, self.eth1_policy_manager, "random")

		self.box_v1 = wx.BoxSizer(wx.VERTICAL)
		self.box_h1 = wx.BoxSizer(wx.HORIZONTAL)

		self.box_h1.Add(self.broadcast_control_box, border=5, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
		self.box_h1.Add(self.scanning_control_box, border=5, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
		self.box_h1.Add(self.random_control_box, border=5, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
		self.box_v1.Add(self.public_message_box, 0, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)
		self.box_v1.Add(self.box_h1, 0, flag=wx.ALL | wx.ALIGN_CENTER_HORIZONTAL)

		self.panel.SetSizer(self.box_v1)
        	self.box_v1.Fit(self)

	def on_timer(self, event):
		##
		# This method is a core process in this program.
		# Everysecond, it calls check_flow to check the whole flows and subsequent
		# procedure automatically detects attack and eventuallly builds blocking/limiting
		# policy depending on the attack status.
		#
		# After that, the methods such as set_~~_text related to the GUI reperesentation
		# are called to mark on the text box. These markings are based on the status_manager.
		# By referring each flag in the status_manager, it sets the text
		#
		# FInally, it clears the attack flag to detect the attack every second.
		##.
		self.eth1_flow_checker.check_flow()
		self.set_public_textbox()
		self.set_broadcast_textbox()
		self.set_scanning_textbox()
		self.set_random_textbox()
		self.clear_attackflag()

	def set_public_textbox(self) :
		self.public_message_box.set_eth1_rate_text(str(self.eth1_flow_counter.get_highest_icmp_flow()))
		self.public_message_box.set_eth1_number_text(str(self.eth1_flow_counter.get_icmp_flow_counts()))

	def set_broadcast_textbox(self) :
		if self.eth1_status_manager.check_flag(100) :
			self.broadcast_control_box.set_eth1_monitor_text("ON")
		else : self.broadcast_control_box.set_eth1_monitor_text("OFF")

		if self.eth1_status_manager.check_flag(1) :
			self.broadcast_control_box.set_eth1_policy_text("OK")
		else : self.broadcast_control_box.set_eth1_policy_text("NONE")

		if self.eth1_status_manager.check_flag(10) :
			self.broadcast_control_box.set_eth1_attack_text("OK")
		else : self.broadcast_control_box.set_eth1_attack_text("NONE")

	def set_scanning_textbox(self) :
		if self.eth1_status_manager.check_flag(200) :
			self.scanning_control_box.set_eth1_monitor_text("ON")
		else : self.scanning_control_box.set_eth1_monitor_text("OFF")

		if self.eth1_status_manager.check_flag(2) :
			self.scanning_control_box.set_eth1_policy_text("OK")
		else : self.scanning_control_box.set_eth1_policy_text("NONE")

		if self.eth1_status_manager.check_flag(20) :
			self.scanning_control_box.set_eth1_attack_text("OK")
		else : self.scanning_control_box.set_eth1_attack_text("NONE")

	def set_random_textbox(self) :
		if self.eth1_status_manager.check_flag(300) :
			self.random_control_box.set_eth1_monitor_text("ON")
		else : self.random_control_box.set_eth1_monitor_text("OFF")

		if self.eth1_status_manager.check_flag(3) :
			self.random_control_box.set_eth1_policy_text("OK")
		else : self.random_control_box.set_eth1_policy_text("NONE")

		if self.eth1_status_manager.check_flag(30) :
			self.random_control_box.set_eth1_attack_text("OK")
		else : self.random_control_box.set_eth1_attack_text("NONE")

	def clear_attackflag(self) :
		self.eth1_status_manager.clear_flag(10)
		self.eth1_status_manager.clear_flag(20)
		self.eth1_status_manager.clear_flag(30)

if __name__ == '__main__':
	app = wx.PySimpleApp()
	app.frame = Frame()
	app.frame.Show()
	app.MainLoop()

